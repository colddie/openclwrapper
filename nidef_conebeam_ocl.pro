;+
; NAME:
;    NIdef_conebeam_ocl
;
; PURPOSE:
;    Define a opencl cone beam projector for use with NIproj. Can be either
;    based on standard ray tracing or rectangular footprints (distance
;    driven).
;
; CATEGORY:
;    Reconstruction
;
; INPUTS:
;    NRCOLS
;    NRROWS
;    NRPLANES: the size of the reconstruction image. Voxels are
;              supposed to be cubic with size 1.
;
;    NRCOLDET
;    NRROWDET: the size of the detector
;
;    DETCOLSIZE
;    DETROWSIZE: the size of the detector, expressed in recon
;                voxel size.
;
;    DETDIST:    distance between rotation center and detector
;
;    FOCUSHEIGHT: distance between focus and detector
;
;    ANGLES:      array with the projection angles
;
; OPTIONAL INPUTS:
;    none
;	
; KEYWORD PARAMETERS:
;    DETCOLCENTER: central column of detector.
;                  Default: (NRCOLDET - 1.0) / 2.0
;
;    DETROWCENTER: central row of the detecto.
;                  Default: (NRROWDET - 1.0) / 2.0
;
;    IMGCOLOFFSET:
;      offset of the detector, expressed in units of reconstruction
;      voxel. Equivalent to DETCOLCENTER -= IMGCOLOFFSET / DETCOLSIZE.
;      Provided because it is typically determined separately in a QC
;      procedure.
;      Default is 0.
;
;    RECONCENTER:
;      Array with 3 elements, center of the cone beam system relative
;      to the image center.
;      Default is ([nrcols,nrrows,nrplanes] - 1.0) / 2.0, which means
;      that system center coincides with the image center.
;
;    FOCUSCOLOFFSET:
;      Offset of the focus in col direction, default is 0.
;      Default of 0 means that the line through focus and image center
;      hit the detector in DETCOLCENTER.
;
;    FOCUSPLANEOFFSET
;      Offset of the focus in plane direction, default is 0.
;      Default of 0 means that the line through focus and image center
;      hit the detector in DETROWCENTER.
;
;    FWHM0:
;      resolution of the detector in detector pixels.
;
;    RAYTRACING
;      When set, the ray tracer projector/backprojector is used,
;      otherwise the distance driven is used.
;      Note that when it is set, the cone beam backprojector can be
;      used to compute Feldkamp reconstruction.
;
;    RIGMOTION : either scalar (= no motion),
;               or an array of 4*4*NRVIEWS,
;               containing the rigid motion that must be
;               compensated during reconstruction, which is 
;               done in the opencl kernel.
;
;    CALIFILE  
;       The calibration file from the scanner which contains the
;       coordinates of the CBCT. See niread_CTcali. When this is
;       specified, the projectors will ignore the default geometry.
;
;    REFPROJD  
;        a reference projector from nidef_conebeam.pro, 
;        which is used to obtain the detetcor corners.
;
;    FORCE_CPU - force opencl code on CPU even if GPU is available
;
;    FAST_MATH - use fast approximation in opencl calculations
;
;    KERNEL_PATH  - the path that contains the opencl kernel file
;
;    UNIT_IN_MM - using when simuating the polychramatic data
;
;    DETBINS  - deprecated
;    SRCLOCS  - deprecated
;
; OUTPUTS:
;    PROJD: a structure describing the projector. The type is either
;    'cbct_rayt_ocl' or 'cbct_distd_ocl', depending on keyword raytracing.
;
; EXAMPLE:
;    
;
; SEE ALSO:
;    Nidef_conebeam, NIdef_projdistd.
;
; MODIFICATION HISTORY:
; 	Written by:	Tao Sun
;-

compile_opt strictarrsubs
Function nidef_conebeam_ocl, califile = califile, $
                    refprojd = refprojd, nrcols, nrrows, nrplanes, $
                    nrcoldet, nrrowdet, $
                    detcolsize  = detcolsize, $
					detrowsize  = detrowsize, $
                    detdist     = detdist, $
				    focusheight = focusheight, $
                    angles      = angles, $		
                    vox_size    = vox_size,  $ ; volume (mm)
                    detcolcenter = detcolcenter, $  ; below are useless?
                    detrowcenter = detrowcenter, $  ;
                    imgcoloffset = imgcoloffset, $  ;COR
                    coloffset    = coloffset,   $
                    rowoffset    = rowoffset,   $
                    planeoffset  = planeoffset, $
                    reconcenter  = reconcenter, $
                    focuscoloffset   = focuscoloffset, $
                    focusplaneoffset = focusplaneoffset, $
                    fwhm0 = fwhm0, $
					raytracing = raytracing, $
					rigmotion  = rigmotion, $
					scale_fac = scale, $              ; not yet implemented for opencl
					scale_det = scale_d, $
					subdet = subdet, $
					force_cpu   = force_cpu, $
				    fast_math   = fast_math, $
				    kernel_path = kernel_path, $
					detbins = detbins, $
					srclocs = srclocs, $
                    unit_in_mm = unit_in_mm				

if not keyword_set(force_cpu) then force_cpu = 0
if not keyword_set(fast_math) then fast_math = 1
					
if not keyword_set(califile) then begin
  if not keyword_set(detdist) then stop
  if not keyword_set(focusheight) then stop
  if not keyword_set(angles) then stop
endif 

if not keyword_set(unit_in_mm) then unit_in_mm = 1.0
if not keyword_set(detcolsize) then detcolsize = 1.0
if not keyword_set(detrowsize) then detrowsize = 1.0
if not keyword_set(vox_size)   then vox_size = [1.0,1.0,1.0]
if n_elements(imgcoloffset)     eq 0 then imgcoloffset = 0.0
if n_elements(focuscoloffset)   eq 0 then focuscoloffset = 0.0
if n_elements(focusplaneoffset) eq 0 then focusplaneoffset = 0.0
if n_elements(fwhm0)            eq 0 then fwhm0 = 0.0
if n_elements(detcoloffset)     eq 0 then detcoloffset = 0.0
if n_elements(detrowoffset)     eq 0 then detrowoffset = 0.0 
if n_elements(reconcenter) eq 0 then begin
  colcenter   = (nrcols - 1.0) / 2.0
  rowcenter   = (nrrows - 1.0) / 2.0
  planecenter = (nrplanes - 1.0) / 2.0
endif else begin
  colcenter   = reconcenter[0]
  rowcenter   = reconcenter[1]
  planecenter = reconcenter[2]
endelse
if n_elements(coloffset)    eq 0   then coloffset = 0.0
if n_elements(rowoffset)    eq 0   then rowoffset = 0.0
if n_elements(planeoffset)    eq 0   then planeoffset = 0.0
if n_elements(detcolcenter) eq 0 $
  then detcolcenter = (nrcoldet - 1.0) / 2.0
if n_elements(detrowcenter) eq 0 $
  then detrowcenter = (nrrowdet - 1.0) / 2.0

if n_elements(rigmotion)        eq 0 then rigmotion = 0.0
  
imgcolwidth  = vox_size[0]
imgrowwidth  = vox_size[1]
imgplanewidth = vox_size[2]


 ; define some parameters
if not keyword_set(califile) then begin
  ;implement explicitly to give detbins, srclocs, deprecated
  refprojd.rigmotion *= 0
  ;refprojd.coloffset *= 0
  ;refprojd.rowoffset *= 0
  ;refprojd.planeoffset *= 0
  detbins      = make_array(4,4,n_elements(angles),value=1.0)
  srclocs      = make_array(4,n_elements(angles),value=1.0)
  img = fltarr(nrcols, nrrows, nrplanes)
  coordinates = {S:fltarr(3), C1:fltarr(3), C2:fltarr(3), C3:fltarr(3), C4:fltarr(3), $
                            V1:fltarr(3), V2:fltarr(3), V3:fltarr(3), V4:fltarr(3), $  
                            U1:fltarr(3), U2:fltarr(3), U3:fltarr(3), U4:fltarr(3), $  							
								  CENTER:fltarr(3), type: ''}
  for i = 0, n_elements(angles)-1 do begin
   ; -----------------------CALL nidistd_proj3d_angle 1 times--------------------
   NIproj_local, img, sino, coordinates,  $
     subset = i, projd = refprojd, test = test      ; shouldn't call test unless realy want table not moving
   srclocs[0:2,i]   = coordinates.S /unit_in_mm
   detbins[0:2,0,i] = coordinates.C1 /unit_in_mm
   detbins[0:2,1,i] = coordinates.C2 /unit_in_mm
   detbins[0:2,2,i] = coordinates.C4 /unit_in_mm
   detbins[0:2,3,i] = coordinates.C3 /unit_in_mm
  endfor  


endif else begin
  niread_CTcali, califile, detbins, srclocs, angles, detdist, focusheight, detcoloffset, detrowoffset
  ;print, 'make sure using dd projector for real data or you need to modify it!'
  if keyword_set(raytracing) then begin
   for iview = 0, n_elements(angles)-1 do begin
    det1 = detbins[0:2,0,iview]
    det2 = detbins[0:2,1,iview]
    det3 = detbins[0:2,2,iview]
    det4 = detbins[0:2,3,iview]
    det1_ = (det3-det1) * 0.5/nrcoldet + det1
	det3_ = (det1-det3) * 0.5/nrcoldet + det3
	det2_ = (det4-det2) * 0.5/nrrowdet + det2
	det4_ = (det2-det4) * 0.5/nrrowdet + det4
    detbins[0:2,0,iview]  = det1_ /unit_in_mm
    detbins[0:2,1,iview]  = det2_ /unit_in_mm
    detbins[0:2,2,iview]  = det3_ /unit_in_mm
    detbins[0:2,3,iview]  = det4_ /unit_in_mm
   endfor
  endif
  ;detcolcenter += 0.0
  ;detrowcenter += 0.0                        ; constant detector offset
  detcolcenter += mean(detcoloffset)/detcolsize
  detrowcenter += mean(detrowoffset)/detrowsize     ; varying detector offset
endelse

 ; define the center, this is indeed the center of the system in the reconstruction,
 ; not the one of the image!
 center        = [0., 0., mean(srclocs[2,*])]
 
 if keyword_set(raytracing) eq 1 then begin
     vox_offset = [(-colcenter-coloffset/vox_size[0]),(-rowcenter-rowoffset/vox_size[1]), (-planecenter-center[2]-planeoffset/vox_size[2]) ] 
	 ;vox_offset = [(-colcenter-coloffset/vox_size[0]),(-rowcenter-rowoffset/vox_size[1]), (-planecenter-planeoffset/vox_size[2]) ] 
 endif else begin
     vox_offset = [(-colcenter-coloffset/vox_size[0]),(-rowcenter-rowoffset/vox_size[1]), (-planecenter-center[2]-planeoffset/vox_size[2])]  $
       -[0.5,0.5,0.5]        ;
	 ;vox_offset = [(-colcenter-coloffset/vox_size[0]),(-rowcenter-rowoffset/vox_size[1]),(-planecenter-planeoffset/vox_size[2])] $
	 ;  -[0.5,0.5,0.5]
 endelse

focuscol   = focuscoloffset                   ; focus offset
focusrow   = focusheight - detdist
focusplane = focusplaneoffset
 
 
 
;-- Projector
  temp = '-D CBCT'
  if keyword_set(rigmotion)    and n_elements(rigmotion) gt 1 then temp += ' -D MC'
  if keyword_set(force_cpu) then temp += ' -D RUN_ON_CPU'
  if keyword_set(fast_math) then temp += ' -D FAST_MATH'
  compile_options = temp
  if not keyword_set(raytracing) then begin
    file_paths      = kernel_path + 'distd_cbct_sinogram.cl' 
  endif else begin
    file_paths      = kernel_path + 'rayt_cbct_sinogram.cl'   
  endelse
  function_names  = 'main_kernel'
  idl_call_names  = 'proj'

 ;-- Backprojector
  temp  =  '-D CBCT -D BACK_PROJECT' ; add '-D BACK_PROJECT when using distd_sinogram.cl
;  temp += ' -D SINO_PIX_X=' + pix_size
;  temp += ' -D PM_REBIN='   + srebin
  if keyword_set(rigmotion)    and n_elements(rigmotion) gt 1 then temp += ' -D MC'
  if keyword_set(force_cpu) then temp += ' -D RUN_ON_CPU'
  if keyword_set(fast_math) then temp += ' -D FAST_MATH'
  compile_options = [compile_options,temp]
  if not keyword_set(raytracing) then begin
    file_paths      = [file_paths, kernel_path + 'distd_cbct_sinogram.cl']
  endif else begin
    file_paths      = [file_paths, kernel_path + 'rayt_cbct_sinogram.cl']  
  endelse
  function_names  = [function_names,'main_kernel']
  idl_call_names  = [idl_call_names,'back']
  
  ;-- Actual bridge creation
  oclbridge = obj_new('niopencl')
  
  b = oclbridge->create_command_queue()

  b = oclbridge->build_kernels(file_paths,     $
                               function_names, $
                               idl_call_names, $
                               compile_options )


if keyword_set(raytracing) then begin

  detcols0      = (findgen(nrcoldet+1) - detcolcenter - 0.5) $
                  * detcolsize   + imgcoloffset
  detrows0      = detcols0 * 0 - detdist
  detplanes0    = (findgen(nrrowdet+1) - detrowcenter-0.5) * detrowsize 
  detcols1      = detcols0 * 0 + focuscoloffset
  detrows1      = detcols0 * 0 + focusheight - detdist
  detplanes1    = detplanes0*0 + focusplaneoffset
  
  
  projdescrip = {type : 'cbct_rayt_ocl', $                                  ; now start the fdk parameters
                 nrangles    : n_elements(angles), $
                 nrcoldet    : nrcoldet, $
                 nrrowdet    : nrrowdet, $
                 nrcols      : nrcols, $
                 nrrows      : nrrows, $
                 nrplanes    : nrplanes, $
                 angles      : float(angles), $
				 unit_in_mm : unit_in_mm, $
                 focuscol    : focuscol/unit_in_mm, $
                 focusrow    : focusrow/unit_in_mm, $
                 focusplane  : focusplane/unit_in_mm, $
                 focusheight : focusheight/unit_in_mm, $
                 detdist     : detdist/unit_in_mm, $
                 colcenter   : colcenter/unit_in_mm, $
                 rowcenter   : rowcenter/unit_in_mm, $
                 planecenter : planecenter/unit_in_mm, $
                 detcols0    : detcols0/unit_in_mm, $
                 detrows0    : detrows0/unit_in_mm, $
                 detplanes0  : detplanes0/unit_in_mm, $
                 detcols1    : detcols1/unit_in_mm, $
                 detrows1    : detrows1/unit_in_mm, $
                 detplanes1  : detplanes1/unit_in_mm, $
				 detcoloffset:  (detcoloffset-mean(detcoloffset))/unit_in_mm, $   ;detcoloffset/unit_in_mm, $   ; this only matters the preweighting for FDK, not projections!
				 detrowoffset:  (detrowoffset-mean(detrowoffset))/unit_in_mm, $    ;detrowoffset/unit_in_mm, $    
				 center      : center/unit_in_mm, $
                 imgcolwidth : imgcolwidth/unit_in_mm, $
                 imgrowwidth : imgcolwidth/unit_in_mm, $
                 imgplanewidth : imgplanewidth/unit_in_mm, $
				 detcolsize   : detcolsize/unit_in_mm, $
				 detrowsize   : detrowsize/unit_in_mm, $
                 fwhm       : fwhm0, $
				 volumefwhm  : 0, $
                 raytracing  : keyword_set(raytracing), $
				 rigmotion   : rigmotion, $
                 ndetcols    : nrcoldet,     $                     ; now start the opencl parameters
                 ndetrows    : nrrowdet,     $
                 reconpix    : vox_size/unit_in_mm,      $
                 vox_offset  : vox_offset, $
                 detbins     : detbins,      $
                 srclocs     : srclocs,     $
                 fast_math     : fast_math,  $
                 force_cpu     : force_cpu,  $
                 oclbridge     : oclbridge,  $
                 kernel_path   : kernel_path      }
endif else begin
  ; Everything is relative to the center of the image,
  ; and distances are in transaxial pixel size.
  ;-----------
  reconcolcenter   = colcenter - (nrcols - 1.0) / 2.0
  reconrowcenter   = rowcenter - (nrrows - 1.0) / 2.0
  reconplanecenter = planecenter - (nrplanes - 1.0) / 2.0
  
  detcols0      = (findgen(nrcoldet+1) - detcolcenter - 0.5) $
                  * detcolsize   + imgcoloffset
  detrows0      = detcols0 * 0 - detdist
  detplanes0    = (findgen(nrrowdet+1) - detrowcenter-0.5) * detrowsize 
  detcols1      = detcols0 * 0 + focuscoloffset
  detrows1      = detcols0 * 0 + focusheight - detdist
  detplanes1    = detplanes0*0 + focusplaneoffset
  imgcol0       = (- nrcols/2.0 - reconcolcenter) * imgcolwidth
  imgrow0       = (- nrrows/2.0 - reconrowcenter) * imgcolwidth
  ;imgcolwidth   = 1.0
  imgplane0     = (-nrplanes/2.0 - reconplanecenter) *imgplanewidth
  ;imgplanewidth = 1.0
  volumefwhm    = 0.0

    projdescrip = {type : 'cbct_distd_ocl', $                    ; now start the fdk parameters
                 nrangles    : n_elements(angles), $
                 nrcoldet    : nrcoldet, $
                 nrrowdet    : nrrowdet, $
                 nrcols      : nrcols, $
                 nrrows      : nrrows, $
                 nrplanes    : nrplanes, $
                 angles      : float(angles), $
				 unit_in_mm : unit_in_mm, $
                 focuscol    : focuscol/unit_in_mm, $
                 focusrow    : focusrow/unit_in_mm, $
                 focusplane  : focusplane/unit_in_mm, $
                 focusheight : focusheight/unit_in_mm, $
                 detdist     : detdist/unit_in_mm, $
                 colcenter   : colcenter/unit_in_mm, $
                 rowcenter   : rowcenter/unit_in_mm, $
                 planecenter : planecenter/unit_in_mm, $
                 detcols0    : detcols0/unit_in_mm, $
                 detrows0    : detrows0/unit_in_mm, $
                 detplanes0  : detplanes0/unit_in_mm, $
                 detcols1    : detcols1/unit_in_mm, $
                 detrows1    : detrows1/unit_in_mm, $
                 detplanes1  : detplanes1/unit_in_mm, $
				 detcoloffset: (detcoloffset-mean(detcoloffset))/unit_in_mm, $    ; this only matters the preweighting for FDK, not projections!
				 detrowoffset: (detrowoffset-mean(detrowoffset))/unit_in_mm, $
                 center      : center/unit_in_mm, $
                 imgcol0     : imgcol0/unit_in_mm, $
                 imgrow0     : imgrow0/unit_in_mm, $
                 imgplane0   : imgplane0/unit_in_mm, $
                 imgcolwidth : imgcolwidth/unit_in_mm, $
                 imgrowwidth : imgcolwidth/unit_in_mm, $
                 imgplanewidth : imgplanewidth/unit_in_mm, $
				 detcolsize   : detcolsize/unit_in_mm, $
				 detrowsize   : detrowsize/unit_in_mm, $
                 fwhm       : fwhm0, $
                 volumefwhm  : 0, $
                 raytracing  : keyword_set(raytracing), $    
				 rigmotion   : rigmotion, $				 
                 ndetcols    : nrcoldet,     $                     ; now start the opencl parameters
                 ndetrows    : nrrowdet,     $
                 reconpix    : vox_size/unit_in_mm,      $
                 vox_offset  : vox_offset, $
                 detbins     : detbins,      $
                 srclocs     : srclocs,     $
                 fast_math     : fast_math,  $
                 force_cpu     : force_cpu,  $
                 oclbridge     : oclbridge,  $
                 kernel_path   : kernel_path      }

endelse
		      
		   
		   
  return, projdescrip

End