; Appending in the beginning when adding a new definition!
;+
; NAME:
;    NIdef_proj
;
; PURPOSE:
;    Create a structure carrying all necessary information about the
;    way of projecting and backprojecting.  The structure serves as
;    input for procedures and functions involved in the reconstruction
;    of images.  
;
; CATEGORY:
;    RECONSTRUCTION
;
; CALLING SEQUENCE:
;    projdescrip = NIdef_proj(...)
;
;    #################################################################
;    # Case of PET3D_HRP or PET3D_ACCEL or PET3D_HIREZ or PET3D_DHRP #
;    #      or PET3D_DTRUEPOINT
;    #################################################################
;    FWHM0
;       The resolution of the PET-system in pixels. If it is a single
;       value, it will be used for transaxial and axial resolution. If
;       it is a two-element array, the first number is the transaxial,
;       the second number the axial resolution. It is assumed that the
;       resolution is the same for all segments.
;       Currently, there are no other parameters. Extension to other
;       geometries is straightforward.
;       Default: perfect resolution.
;       Note: it is recommended to use VOLUMEFWHM to model the
;       resolution.
;
;    VOLUMEFWHM
;       The resolution of the PET-system, modeled as a 3D Gaussian
;       convolution in the reconstruction volume. 
;       If it is a single value, it is used in all three dimensions.
;       If it is an array with two elements, the first is used
;       transaxially, the second axially.
;       It is expressed with unit (transaxial or axial) "pixel size".
;       It may be a bit slower than when using FWHM0 to model the
;       detector resolution. However, it results in better modelling,
;       in particular if one uses small voxels. The projector uses ray
;       tracing, and if small voxels are used, the volume sampling is
;       relatively poor. However, it that poor sampling is followed by
;       a smoothing which is large compared to the sampling, the
;       sampling artifacts are strongly reduced. This stabilizing
;       effect is not obtained when smoothing the projections.
;
;    SUMDET
;       The number of detectors summed to produce a single sinogram
;       value. Default is 1. Often a value of 2 is used to obtain a
;       sinogram of 144x144 for the HR+.
;
;    NRPLANES
;       The number of detector rings, including the "virtual" ones
;       (direct planes and cross planes). Default is 63 (HR+).
;
;    NRDET
;       The number of columns in a sinogram. Default is 288 / sumdet.
;
;    NRCOLS
;       The number of columns in the reconstruction. Default is
;       288/sumdet.
;
;    RELRECONSIZE
;       The reconstruction pixel size relative to the sinogram pixel
;       size. Default is 1. With a value larger than 1, the "medium"
;       resolution reconstruction is directly computed from the high
;       resolution data, which is much faster than doing this in two
;       subsequent steps. When modeling resolution, it can be used to
;       reconstruct on a denser grid as well (relreconsize < 1). This
;       is only for the transaxial resolution. See RELRECONPLANESIZE
;       for the axial resolution.
;
;    RELRECONPLANESIZE
;       The reconstruction plane size relative to the sinogram plane
;       size. Default is 1.
;       The default number of reconstructed planes is then 
;       ceil(nrplanes / relreconplanesize).
;
;    IMGCOLOFFSET
;       An offset of the sinogram center relative to the projection of
;       the rotation axis into the sinogram. For these PET systems,
;       this is half a crystal, obtained by setting IMGCOLOFFSET =
;       -0.5.  This is also the default value.
;
; OUTPUTS:
;    The strucrure projdescrip.
;
; OPTIONAL OUTPUTS:
;    None
;
; COMMON BLOCKS:
;    None
;
; SIDE EFFECTS:
;    None
;
; PROCEDURE:
;    Depending on the requested type of projection/backprojection, the
;    structure is formed in this program, or a call is made to another
;    program to do the job.
;
; EXAMPLE:
;    None
;
; SEE ALSO:


function NIdef_rayt_hirez_ocl, nrdet       = nrdet,          $
                     nrangles        = nrangles,       $ 
                     nrcols          = nrcols,         $
                     nrrows          = nrrows,         $
                     nrplanes        = nrplanes,       $
                     startangle      = startangle,     $
                     deltaangle      = deltaangle,     $
                     angles          = angles,         $
                     bilinear        = bilinear,       $
                     fov_radius      = fov_radius,     $
                     arc_radius      = arc_radius,     $
                     detdist         = detdist,        $
                     fwhm0           = fwhm0,          $
                     fwhm_slope      = fwhm_slope,     $
                     sumdet          = sumdet,         $
                     nrcoldet        = nrcoldet,       $
                     nrrowdet        = nrrowdet,       $
                     detcolsize      = detcolsize,     $
                     detrowsize      = detrowsize,     $
                     detpixsize      = detpixsize,      $
                     pinheight       = pinheight,      $
                     imgcoloffset    = imgcoloffset,   $
                     imgrowoffset    = imgrowoffset,   $
                     focuscoloffset  = focuscoloffset, $
                     focusrowoffset  = focusrowoffset, $
                     forwardrotation = forwardrotation, $
                     dettilt         = dettilt,        $
                     paridl          = paridl,         $
                     nrhead          = nrhead,         $
                     nraper          = nraper,         $
                     nrrays          = nrrays,          $
                     raycoloffset    = raycoloffset,    $
                     rayrowoffset    = rayrowoffset,    $
                     rayweights      = rayweights,      $
                     diameter        = diameter,        $
                     dettwist        = dettwist,        $
                     detproj         = detproj,         $
                     deviations      = deviations,      $
                     deffunc         = deffunc,         $
                     aperincl        = aperincl,        $
                     penetration     = penetration,    $
                     acceptance      = acceptance,     $
                     attcoef         = attcoef,        $
                     refined         = refined,        $
                     fcoloff         = fcoloff,        $
                     frowoff         = frowoff,        $
                     fplaoff         = fplaoff,        $
                     modelaper       = modelaper,      $
                     maskimg         = maskimg,        $
                     subsampling     = subsampling,    $
                     attencorr       = attencorr,      $
                     gaussdif        = gaussdif,       $
                     cgaussdif       = cgaussdif,      $
                     parkul          = parkul,         $
                     pet3d_hrp       = pet3d_hrp,      $
                     pet3d_accel     = pet3d_accel,    $
                     pet3d_hirez     = pet3d_hirez,    $
                     pet3d_span      = pet3d_span,     $
                     pet3d_flat      = pet3d_flat,     $
                     pet3d_focus     = pet3d_focus,    $
                     pet3d_dhrp      = pet3d_dhrp,     $
                     pet3d_dchrp     = pet3d_dchrp,     $	
                     pet3d_distd_hrp_ocl    = pet3d_distd_hrp_ocl, $			
                     pet3d_rayt_hrp_ocl     = pet3d_rayt_hrp_ocl, $	 
                     pet3d_dtruepoint  = pet3d_dtruepoint, $
					           pet3d_rayt_hirez_ocl   = pet3d_rayt_hirez_ocl, $
                     fanbeam         = fanbeam,        $
                     focusheight     = focusheight,    $
                     relreconsize    = relreconsize,   $
                     relreconplanesize = relreconplanesize, $
                     nrsegments      = nrsegments,     $
                     span            = span,           $
                     totnrdet        = totnrdet,       $
                     planeoffsets    = planeoffsets,   $
                     volumefwhm      = volumefwhm,     $
                     only_segment0   = only_segment0,  $
                     tof             = tof,            $
                     cmptof          = cmptof,         $
                     rot_based       = rot_based,      $
                     timeres         = timeres,        $
                     binres          = binres,         $
                     nrbins          = nrbins,         $
                     bin_offset      = bin_offset,    $
                     gaussprecision  = gaussprecision, $
                     scannerdiameter = scannerdiameter,$                     
                     pet3d_mct       = pet3d_mct,      $
                     nrringblocks    = nrringblocks,   $
                     true_axialsampling = true_axialsampling, $
                     pet2d_breast    = pet2d_breast,   $
                     scannertype     = scannertype,    $
                     projectortype   = projectortype,  $
                     detsplitfactor  = detsplitfactor, $
                     detpixratio     = detpixratio,    $
                     pet3d_mmr       = pet3d_mmr,      $
                     fasttof         = fasttof,        $
                     sigma           = sigma,          $
                     child           = child,          $
                     family          = family,         $
                     logfile         = logfile,        $
                     nrbedpositions  = nrbedpositions, $
                     segments        = segments,       $
                     singlesrate     = singlesrate,    $
                     resolutionmodelling = resolutionmodelling, $
                     true_segmentsampling = true_segmentsampling, $
                     siemensdefault = siemensdefault, $
					           fast_math         = fast_math, $                 ; add later
					           force_cpu         = force_cpu, $
					           rigmotion         = rigmotion, $
					           ;initial_coords = initial_coords, $
					           kernel_path       = kernel_path, $
                     vox_offset      = vox_offset							   

NIpath_kernels = kernel_path

if n_elements(rigmotion)   eq 0 then rigmotion = 0.0            ; avoid improper define


if not keyword_set(pet3d_hrp) and not keyword_set(pet3d_accel) and $
  not keyword_set(pet3d_span) and not keyword_set(pet3d_flat) and $
  not keyword_set(pet3d_focus) and not keyword_set(pet3d_hirez) and $
  not keyword_set(pet3d_dhrp) and not keyword_set(pet3d_dtruepoint) and $
  not keyword_set(pet3d_mct) and not keyword_set(pet2d_breast) and $
  not keyword_set(pet3d_mmr) and not keyword_set(pet3d_dchrp) and $
  not keyword_set(pet3d_dhrp_ocl) and not keyword_set(pet3d_rayt_hrp_ocl) and $
  not keyword_set(pet3d_rayt_hirez_ocl) then begin
    if n_elements(angles) eq 0 then begin
        if n_elements(startangle) eq 0 then startangle = 0.0
        if n_elements(nrangles)   eq 0 $
          then if n_elements(detdist) gt 1 $
            then nrangles = n_elements(detdist) $
        else begin
            print, 'NIdef_proj: nrangles should be supplied'
            return, -1
        endelse
        if n_elements(deltaangle) eq 0 then deltaangle = !pi/nrangles
        angles = startangle + findgen(nrangles) * deltaangle
    endif else begin
        nrangles   = n_elements(angles)
        startangle = angles[0]
    endelse
endif

case 1 of 
	  
	  ;=========================================
    keyword_set(pet3d_rayt_hirez_ocl) : begin
      ; Same as above, but now for distance driven (back)proj.
      ; Define the geometry of the HR+.
      ; Only difference is projdescrip.type: so this can be merged
      ; by using "keyword_set(pet3d_hrp) or keyword_set(pet3d_dhrp)".
      ;===

      pixelsizecm = 0.2
      if n_elements(sumdet) eq 0 then sumdet = 1
      if n_elements(relreconsize)      eq 0 then relreconsize = 1
      if n_elements(relreconplanesize) eq 0 then relreconplanesize = 1.0
      pixelsizecm = pixelsizecm * sumdet * relreconsize ; sum detectors
      ; doi = 9.4  ; to be checked
      ; radius = (854.8 / 2.0 + doi) /10. / pixelsizecm 
      radius = 42.7808 /pixelsizecm
      totnrdet = 1344 / sumdet
      if n_elements(nrdet)  eq 0 then nrdet = 336 / sumdet
      if n_elements(nrcols) eq 0 then nrcols = totnrdet / 4
      nrdetplanes = 81
      if not keyword_set(only_segment0) then begin
        ;offsets     = [22, 11, 0, -11, -22]
        offsets     = [44, 22, 0, -22, -44]
        segplanes   = [47, 69, 81, 69, 47]
        nrsegments  = 5
      endif else begin
        span        = 0
        offsets     = [0]
        segplanes   = [81]
        nrsegments  = 1
      endelse
      nrangles    = 336            ; mesh of two.
      if n_elements(imgcoloffset) eq 0 then begin
        ;angleoffset  = - !pi / totnrdet
        angleoffset  = - !pi / 1344
      endif else begin
        angleoffset  = imgcoloffset * 2 *!pi / totnrdet
      endelse
	  
      if n_elements(nrplanes) eq 0 $
        then nrplanes = fix(ceil(nrdetplanes / float(relreconplanesize)))
		  
		FOVcenter_x = (nrcols-1) / 2.0
		if keyword_set(siemensdefault) then FOVcenter_x += ( 1.0/2 / sumdet )
		FOVcenter_y = (nrcols-1) / 2.0
		if keyword_set(siemensdefault) then FOVcenter_y += ( 1.0/2 / sumdet )
		FOVcenter_z = (nrplanes - 1.0) / 2.0
		
      ; Call NIpet3dcoords to obtain the structure petstruct
      ;=====
      dummy = NIpet3dcoords(petstruct, initpet='hirez', $
                            ecatradius=       radius,$
                            ecatnrdet=        nrdet, $
                            ecattotdet=       totnrdet, $
                            ecatnrangles=     nrangles, $
                            ecatcentercol=    FOVcenter_x, $
                            ecatcenterrow =   FOVcenter_y, $
                            ecatcenterplane = FOVcenter_z, $
                            ecatrecplanesize = relreconplanesize, $
                            ecatspan         = span, $
                            ecatangleoffset  = angleoffset, $
                            ecatnrplanes =    nrdetplanes, $
                            ecatplaneoffsets= offsets, $
                            ecatnrsegplanes  = segplanes, $
                            hirezplanesepar  = 1.0 / sumdet)


	if n_elements(fwhm0) eq 0 then fwhm0 = 0.0
	if n_elements(volumefwhm) eq 0 then volumefwhm = 0.0
		
		
	; coordnates for the kernel, projector segments arrangement [0,-1,1,-2,2], which needs to transfer to [0,1,2,3,4]
    if nrsegments gt 1 then begin
      more     = nrsegments-1
      segments = intarr(nrsegments)
      dummy    = indgen( (nrsegments-1)/2 )
      segments[2 * dummy + 1] = dummy + 1
      segments[2 * dummy + 2] = - (dummy + 1)
    endif else begin
      segments = [0]
    endelse

  plane1 = fltarr(nrdetplanes, nrsegments)
  plane2 = fltarr(nrdetplanes, nrsegments)
  segplanes_ = segplanes * 0.
  for i_seg = 0, nrsegments-1 do begin
    segment = segments[i_seg]
    segindex = segment +(n_elements(segments)-1)/2
    for plane = 0, segplanes[segindex]-1 do begin
      dummy = NIpet3dcoords(petstruct, $
        col1, row1, curplane1, col2, row2, curplane2, $
        angle = 0., plane=plane, segment=segment)
      plane1[plane, segindex] = mean(curplane1) ;# make_array(nrdetplanes, /int, value=1)
      plane2[plane, segindex] = mean(curplane2) ;# make_array(nrdetplanes, /int, value=1)
    endfor
    viewsize = segplanes[segindex]             ; reoder the segplanes to [63, 53, 53, 35,35]
    segplanes_[i_seg] = viewsize
  endfor 
  segplanes = segplanes_
;stop
;    segplanes = intarr(n_elements(segments))
;    for i_seg = 0, n_elements(segments)-1 do begin
;      viewsize = NIpetstruct_distd(petstruct, $
;        segment= segments[i_seg], /get_viewsize)
;      segplanes[i_seg] = viewsize[1]
;    endfor
;    segstartplane = intarr(n_elements(segments))
;    segstopplane  = intarr(n_elements(segments))
;    for i_seg= 1, n_elements(segments)-1 do $
;      segstartplane[i_seg] = segstartplane[i_seg - 1] + segplanes[i_seg-1]
;    segstopplane = segstartplane + segplanes - 1
    
    
    npoints = nrdet * total(segplanes)                                                ;npoints = (nrdet+1)*total(segplanes+nrsegments)
    initial_coords1 =  make_array(4, npoints, /float, value=1.0)
    initial_coords2 = initial_coords1
    currentpoint = 0.0
    for i_seg = 0, n_elements(segments)-1 do begin
    
      segment = segments[i_seg]
      segindex = segment +(n_elements(segments)-1)/2
      ;nrplanes_sino = n_elements(where(petstruct.plane1[0:segplanes[i_seg], segindex] ne 0.0))
      nrplanes_sino = segplanes[i_seg]
      ;stop
      ; first corner, then source
      npoint = nrdet*nrplanes_sino            ; center, source and detector
      initial_coords1[0,currentpoint:currentpoint+npoint-1]  = col1 # (fltarr(1,nrplanes_sino)+1)
      initial_coords1[1,currentpoint:currentpoint+npoint-1]  = row1 # (fltarr(1,nrplanes_sino)+1)
      initial_coords1[2,currentpoint:currentpoint+npoint-1]  = (fltarr(nrdet,1)+1) # reform(plane1[0:segplanes[i_seg]-1,segindex],1,nrplanes_sino)
      
      initial_coords2[0,currentpoint:currentpoint+npoint-1]   = col2 # (fltarr(1,nrplanes_sino)+1)
      initial_coords2[1,currentpoint:currentpoint+npoint-1]   = row2 # (fltarr(1,nrplanes_sino)+1)
      initial_coords2[2,currentpoint:currentpoint+npoint-1]   = (fltarr(nrdet,1)+1) # reform(plane2[0:segplanes[i_seg]-1,segindex],1,nrplanes_sino)
      
      ;initial_coords[*,ncurrentpoint:ncurrentpoint+npoint-1] = initial_coord
      currentpoint += npoint
    endfor
    
    npoints = nrdet*total(segplanes)*2  + 1
    initial_coords =  make_array(4, npoints, /float, value=1.0)
    ; the center has to inherit from the opencl projector
    initial_coords[0:2,0]   = [petstruct.centercol, petstruct.centerrow, petstruct.centerplane] ; 0. for spiral ct
    initial_coords[3,0]     = 0.
    initial_coords[*,1:(npoints-1)/2] = initial_coords1
    initial_coords[*,(npoints-1)/2+1:npoints-1] = initial_coords2


		;stop
		
		
		; Define the bridge
		;-- Projector
		temp = '-D HRPLUS -D CBCT'
		if keyword_set(rigmotion) then temp += ' -D MC'
		if keyword_set(force_cpu) then temp += ' -D RUN_ON_CPU'
		if keyword_set(fast_math) then temp += ' -D FAST_MATH'
		compile_options = temp
		file_paths  = NIpath_kernels + 'rayt_sinogram_hrplus.cl'
		function_names  = 'main_kernel'
		idl_call_names  = 'proj'
		
		;-- Backprojector
		temp  =  '-D HRPLUS -D CBCT'
		temp += ' -D BACK_PROJECT'
		;  temp += ' -D SINO_PIX_X=' + pix_size
		;  temp += ' -D PM_REBIN='   + srebin
		if keyword_set(rigmotion) then temp += ' -D MC'
		if keyword_set(force_cpu) then temp += ' -D RUN_ON_CPU'
		if keyword_set(fast_math) then temp += ' -D FAST_MATH'
		compile_options = [compile_options,temp]
		file_paths  = [file_paths, NIpath_kernels + 'rayt_sinogram_hrplus.cl']
		function_names  = [function_names,'main_kernel']
		idl_call_names  = [idl_call_names,'back']
		
		
		;-- Actual bridge creation
		oclbridge = obj_new('niopencl')
		
		b = oclbridge->create_command_queue()
		
		b = oclbridge->build_kernels(file_paths,     $
		  function_names, $
		  idl_call_names, $
		  compile_options )
		  
		  
		  
		projdescrip = {type              : 'pet3d_rayt_hirez_ocl', $
		  nrangles          : nrangles, $
		  nrdet             : nrdet, $
		  nrcols            : nrcols, $
		  nrrows            : nrcols, $
		  nrplanes          : nrplanes, $
		  nrsegments        : nrsegments, $
		  petstruct         : petstruct, $
		  fwhm              : fwhm0, $
		  volumefwhm        : volumefwhm, $
		  relreconsize      : relreconsize, $           ; add later
		  relreconplanesize : relreconplanesize, $
		  ;col1              : col1, $
      ;col2              : col2, $
      ;row1              : row1, $
      ;row2              : row2, $
      ;plane1            : plane1, $
      ;plane2            : plane2, $
		  oclbridge         : oclbridge, $
		  rigmotion         : rigmotion, $
		  vox_offset        : vox_offset, $
		  initial_coords    : initial_coords}
end

endcase

return, projdescrip

end
