;+ 
; NAME:
;    niproj_distd_conebeam_ocl
;
; PURPOSE:
;    distance-driven opencl reconstruction for conbeam ct, call the opencl kernel.
;
; CATEGORY:
;    
;
; INPUTS:
;    IMAGE
;      when BACKPROJECT is not set, IMAGE is projected into SINOGRAM.
;
;    SINOGRAM
;      when BACKPROJECT is set, SINONGRAM is backprojected into IMAGE             
;
; OPTIONAL INPUTS:
;
;    PROJDESCRIP:
;      a structure produced with NIdef_conebeam, which in turn may
;      have been called by NIdef_proj. PROJDESCRIP.type should be
;      'distd'.
;
;    BACKPROJECT:
;      when set, a backprojection is computed. Otherwise a projection
;      is computed.
;
;    SUBSET:
;      an array with the indices of the projections to be used for the
;      (back)projection. Default is all angles.
;
;    SUBONLY
;      when set, input and output for the (back)projection have the
;      size of sinogram[*,subset,*]. Make sure all other sinogram-like
;      variable (e.g. attenuation) are also given for subsets angles only.
;
;    NEW:
;      when set, the output array (IMAGE or SINOGRAM) is set to zero
;      before the (back)projection.
;
;     EXPMIN               - deprecated
;     HOLES                - for patch recon
;     WHERE_HOLES  - for patch recon
;
; OUTPUTS:
;    IMAGE:     see INPUTS
;    SINOGRAM:  see INPUTS
;  
; EXAMPLE:
;    
;
; SEE ALSO:
;    niproj, nidef_conebeam_ocl, distd_cbct_sinogram.cl
;
; MODIFICATION HISTORY:
;   Written by: Tao Sun 2017
;-

compile_opt strictarrsubs

Pro niproj_distd_conebeam_ocl,      image,                     $
                          sinogram,                  $
                          projdescrip = projdescrip, $
                          backproject = backproject, $
                          expmin      = expmin,      $
                          subset      = subset,      $
                          subonly     = subonly,     $
                          new         = new, holes=holes, where_holes=where_holes
						  
						  
  if projdescrip.type NE 'cbct_distd_ocl' then begin
     print, 'NIproj_cbct_ocl: projdescrip-type should be "cbct_distd_ocl"'
     stop
  endif

  ; Find fwhm_t and fwhm_a (transaxial and axial)
;-------
fwhm_t = float(projdescrip.fwhm[0])
if n_elements(projdescrip.fwhm) lt 2 $
  then fwhm_a = fwhm_t $
else fwhm_a = float(projdescrip.fwhm[1])

; Find fwhm_xy and fwhm_z (volumetric resolution modelling)
;-------
fwhm_xy = projdescrip.volumefwhm[0]
if n_elements(projdescrip.volumefwhm) eq 2 $
  then fwhm_z = projdescrip.volumefwhm[1] $
else fwhm_z = fwhm_xy


  ;== Set some variables
  true   = 1
  false  = 0
  bridge = projdescrip.oclbridge
  voxdim = projdescrip.reconpix
						  
  if n_elements(subset) eq 0 then subset = indgen(projdescrip.nrangles)
  nrangles = n_elements(subset)

  ;== Initialize image and/or sinogram
  if keyword_set(backproject) then begin
     if (n_elements(image) LE 1)  OR (keyword_set(new)) then    $
        image = fltarr(projdescrip.nrcols,                      $
                       projdescrip.nrrows,                      $
                       projdescrip.nrplanes)
  endif else begin
     if (n_elements(sinogram) LE 1)  OR (keyword_set(new)) then begin
	  if keyword_set(subonly) $
        then sinogram = fltarr(projdescrip.ndetcols, nrangles, $
                      projdescrip.ndetrows ) $
        else sinogram = fltarr(projdescrip.ndetcols, projdescrip.nrangles, $
                      projdescrip.ndetrows )
	 endif
  endelse
  
  if keyword_set(subonly)              $
    then sinoproj = sinogram           $
    else sinoproj = sinogram[*,subset,*]

  niswyz, sinoproj
  
   if keyword_set(backproject) then begin
  
    ;tmpimage = image * 0.0
    ; resolution in sinogram
    if fwhm_t gt 0 $
      then sinogram = NIconvolgauss(sinogram, fwhm = fwhm_t, dim=0)
    if fwhm_a gt 0 $
      then sinogram = NIconvolgauss(sinogram, fwhm = fwhm_a, dim=1)
    
  endif else begin
    
    ; resolution modelling
    if fwhm_xy gt 0 or fwhm_z gt 0 then begin
      image = NIconvolgauss(image, fwhm = fwhm_xy, dim = [0,1], nrsig = 3)
      image = NIconvolgauss(image, fwhm = fwhm_z, dim = [2], nrsig = 3)
    endif else begin
      ;tmpimage = image
    endelse
  endelse
  
  ;== Some fixed input parameters
  center     = [projdescrip.center, 1UL]
  size_img   = [(size(image))[1:3],1UL]
  size_sino  = [projdescrip.ndetcols,projdescrip.ndetrows,nrangles,1UL]
  vox_size   = [voxdim, 0.]
  img_offset = [projdescrip.vox_offset * vox_size, 1UL]
  ;img_offset = - size_img * vox_size / 2.

  detbins  = projdescrip.detbins[*,*,subset]
  srclocs  = projdescrip.srclocs[*,subset]	
  
  rigmotion = fltarr(4,4,nrangles)
  for ii = 0, nrangles-1 do begin  
    ;rigmotion[*,*,ii] = projdescrip.rigmotion[*,*,subset[ii]]	
	if n_elements(projdescrip.rigmotion) gt 1  then begin
      dof = projdescrip.rigmotion[*,subset[ii]]
      rigmotion[*,*,ii] = NImotion2matrix(motion=dof, /homo)               ; not the one in IDL!    
    endif else begin
      rigmotion = 0.
    endelse
  endfor
 
						  
	;== Allocate buffers & write data
  bptr_image   =  0L
  bptr_sino    =  1L
  bptr_detbins =  2L
  bptr_srclocs =  3L
  bptr_mc      =  4L
  ;bptr_debug   =  5L

  b = bridge->create_buffer(bptr_image,   image,    0, 0)
  b = bridge->create_buffer(bptr_sino,    sinoproj, 0, 0)
  b = bridge->create_buffer(bptr_detbins, detbins,  2, 0)
  b = bridge->create_buffer(bptr_srclocs, srclocs,  2, 0)
  b = bridge->create_buffer(bptr_mc, rigmotion, 2,0)
  ;output = fltarr(4, nrangles)   ;sinoproj                        ; shouldn't turn on if too many views at one time
  ;output = fltarr(6)
  ;b = bridge->create_buffer(bptr_debug, output,  0, 0)       ;write_only;

  if keyword_set(backproject) then begin
     kernel = 'back'
  endif else begin
     if keyword_set(expmin) then begin
        kernel = 'projexpmin'
     endif else begin
        kernel = 'proj'
     endelse
  endelse

  ;== Set kernel arguments
  b = bridge->set_kernel_arg(kernel, 0, bptr_image,   true)
  b = bridge->set_kernel_arg(kernel, 1, bptr_sino,    true)
  b = bridge->set_kernel_arg(kernel, 2, bptr_detbins, true)
  b = bridge->set_kernel_arg(kernel, 3, bptr_srclocs, true)

  b = bridge->set_kernel_arg(kernel, 4, center,     false)  
  b = bridge->set_kernel_arg(kernel, 5, size_img,   false)
  b = bridge->set_kernel_arg(kernel, 6, size_sino,  false)
  b = bridge->set_kernel_arg(kernel, 7, img_offset, false)
  b = bridge->set_kernel_arg(kernel, 8, vox_size,   false)
  b = bridge->set_kernel_arg(kernel, 9, bptr_mc, true)
  ;b = bridge->set_kernel_arg(kernel, 10, bptr_debug, true)
  
  ;== Actual work
  global = ulong(size_sino[0:2])
  local  = ulong([0,0,0])


  b = bridge->execute_kernel(kernel, global, local, false)

  ;== Read out data
  if keyword_set(backproject) then begin
     b = bridge->read_buffer(bptr_image, image)
     image /= nrangles
	 
   ; resoultion modelling
   if fwhm_xy gt 0 or fwhm_z gt 0 then begin
      image = NIconvolgauss(image, fwhm = fwhm_xy, dim = [0,1], nrsig = 3)
      image = NIconvolgauss(image, fwhm = fwhm_z, dim = [2], nrsig = 3)
   endif
   
  ; erase part when keyword_set(holes)
  if keyword_set(holes) then begin
    if size(where_holes,/type) ne 10 then begin
      nrholes = n_elements(where_holes[0,*])
      for nrh = 0, nrholes-1 do begin
        image[where_holes[0, nrh]:where_holes[1, nrh], $
              where_holes[2, nrh]:where_holes[3, nrh], $
              where_holes[4, nrh]:where_holes[5, nrh]] = 0.
      endfor
    endif else begin
      for pc = 0, n_elements(where_holes)-1  do $
        image(*where_holes[pc]) = 0
    endelse
  endif
  ;if keyword_set(holes) then stop
	 
  endif else begin
     b = bridge->read_buffer(bptr_sino, sinoproj)
     niswyz, sinoproj
	 
	  ; resolution in sinogram
      if fwhm_t gt 0 $
      then sinoproj = NIconvolgauss(sinoproj, fwhm = fwhm_t, dim=0)
      if fwhm_a gt 0 $
      then sinoproj = NIconvolgauss(sinoproj, fwhm = fwhm_a, dim=1)
	  
     if keyword_set(subonly)                $
       then sinogram             = sinoproj $
       else sinogram[*,subset,*] = sinoproj
  endelse

  ;b = bridge->read_buffer(bptr_debug, output)
  
  ;== Clean up buffers
  b = bridge->release_buffer(bptr_image)
  b = bridge->release_buffer(bptr_sino)
  b = bridge->release_buffer(bptr_detbins)
  b = bridge->release_buffer(bptr_srclocs)	
  b = bridge->release_buffer(bptr_mc)  
  ;b = bridge->release_buffer(bptr_debug)
						  
						  
			  
					  
End