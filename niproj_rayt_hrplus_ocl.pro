;+
; NAME:
;    NIproj_dhrplus
;
; PURPOSE:
;    projection and backprojection for HR+ in 3D using
;    NIdistd_proj3d (distance driven in 2D).
;
; CATEGORY:
;    reconstruction
;
; CALLING SEQUENCE:
;    NIproj_hrplus, image, sinogram, projdescrip, $
;                   back = back, subset = subset
;
; INPUTS:
;    For projection, IMAGE is the input.
;    For backprojection, SINOGRAM is the input
;
;    Sinogram is a volume, obtained by concatenating the segments 0,
;    1, -1, 2 and -2 in the third dimension. So the sinogram size is
;    nrdet x nrangles x (planes_0 + planes_1 + ...).
;
; KEYWORD PARAMETERS:
;    projdescrip : structure as produced by NIdef_proj. Mandatory.
;
;    back: when set, backprojection is performed, otherwise
;          projection.
;
;    subset: an array with the indices of the angles to be used in the
;            operation. The default is to use them all.
;
;    attenuation: when supplied, the attenuation is applied before
;          detector smoothing (fwhm) during the projection and after
;          detector smoothing during backprojection.
;
; OUTPUTS:
;    For projection, SINOGRAM is the output.
;    For backprojection, IMAGE is the output
;    
;
; RESTRICTIONS:
;    Only executes when projdescrip.type equals 'pet3d_dhrp', which is
;    ok if you use NIdef_proj with the keyword PET3D_DHRP.
;
; PROCEDURE:
;    Takes care of the sinogram smoothing, scans the angles of the
;    subset and calls NIdistd_proj3d_1angle to do the actual
;    (back)projection.
;
; SEE ALSO:
;    NIproj, NIdef_proj.
;
; MODIFICATION HISTORY:
; 	Written by:	Johan Nuyts, nov 2009
;-
pro NIproj_rayt_hrplus_ocl, image, sinogram, projdescrip, backproject=backproject, $
                    subset = subset, new = new, $
                    attenuation = attenuation, $
                    calctime = calctime

startt = systime(1)
calctime2 = dblarr(2)

if total(projdescrip.type eq $
         ['pet3d_rayt_hrp_ocl']) eq 0 then begin
  print, 'NIproj_dhrplus: projdescrip.type should be ' + $
         '<pet3d_rayt_hrp_ocl>, but it was ' + NIstring(projdescrip.type)
  return
endif

; Use all angles if no subset is specified.
;------
if n_elements(subset) eq 0 $
  then subset = indgen(projdescrip.nrangles)

; Allocate images if they are not yet available
;------
if keyword_set(backproject) then begin
    if n_elements(image) le 1 $
      then image = fltarr(projdescrip.nrcols, projdescrip.nrrows, $
                          projdescrip.nrplanes) 
endif else begin
    if n_elements(sinogram) le 1 $
      then sinogram = fltarr(projdescrip.nrdet, projdescrip.nrangles, $
                             total(projdescrip.petstruct.segplanes))
endelse

; Erase if requested.
;-------
if keyword_set(new) then $
  if keyword_set(backproject) then image    = image    * 0.0 $
                              else sinogram = sinogram * 0.0

; Find fwhm_t and fwhm_a (transaxial and axial)
;-------
fwhm_t = float(projdescrip.fwhm[0])
if n_elements(projdescrip.fwhm) lt 2 $
  then fwhm_a = fwhm_t $
  else fwhm_a = float(projdescrip.fwhm[1])

; Find fwhm_xy and fwhm_z (volumetric resolution modelling)
;-------
fwhm_xy = projdescrip.volumefwhm[0]
if n_elements(projdescrip.volumefwhm) eq 2 $
  then fwhm_z = projdescrip.volumefwhm[1] $
  else fwhm_z = fwhm_xy

; some initializations
;------
if projdescrip.nrsegments gt 1 then begin
  more     = projdescrip.nrsegments-1
  segments = intarr(projdescrip.nrsegments)
  dummy    = indgen( (projdescrip.nrsegments-1)/2 )
  segments[2 * dummy + 1] = dummy + 1
  segments[2 * dummy + 2] = - (dummy + 1)
endif else begin
  segments = [0]
endelse


segplanes = projdescrip.petstruct.segplanes
segstartplane = intarr(n_elements(segments))
segstopplane  = intarr(n_elements(segments))
for i_seg= 1, n_elements(segments)-1 do $
  segstartplane[i_seg] = segstartplane[i_seg - 1] + segplanes[i_seg-1]
segstopplane = segstartplane + segplanes - 1

;stop

;== Set some variables
true   = 1
false  = 0
bridge = projdescrip.oclbridge
size_img   = [(size(image))[1:3],1UL]                        ;
;size_sino  = [(size(sinogram))[1],segplanes[0],(size(sinogram))[2],1UL]
;vox_size   = [projdescrip.relreconsize, projdescrip.relreconsize, projdescrip.relreconplanesize , 1.]
vox_size   = [projdescrip.pixelsizemm, projdescrip.pixelsizemm, projdescrip.planesepmm , 1.]
img_offset = [projdescrip.vox_offset*vox_size, n_elements(segments)] * 1.  ;- size_img * vox_size / 2.
;add_offset_seg = [[0.5, 0.5, 0.5]*0., n_elements(segments)]      ; the offset specified in the projector already
detbins0 = projdescrip.initial_coords
;center    = projdescrip.initial_coords[*,0]
;srclocs0  = projdescrip.initial_coords[*,1:((size(projdescrip.initial_coords))[2]-1)/2]
;detbins0  = projdescrip.initial_coords[*,((size(projdescrip.initial_coords))[2]-1)/2+1 : (size(projdescrip.initial_coords)-1]


;stop

nrangles  = n_elements(subset)
srclocs0  = fltarr(4,nrangles)
rigmotion = fltarr(4,4,nrangles)


for ii = 0, nrangles-1 do begin
  ;srclocs0[0,ii] = projdescrip.petstruct.angles[subset[ii]]
  srclocs0[0,ii] = !pi * float(subset[ii]) / float(projdescrip.nrangles)
  ;srclocs0[1,ii] = mean(projdescrip.tablepos[subset[ii], *])
 
  if n_elements(projdescrip.rigmotion) gt 1  then begin
    rigmotion[*,*,ii] = projdescrip.rigmotion[*,*,subset[ii]]
  endif else begin
    ;    rigmotion = 0.
  endelse
  
endfor


;i_seg = 0
;if keyword_set(subonly)              $
;  then sinoproj = sinogram           $
;else sinoproj = sinogram[*,subset,segstartplane[i_seg]:segstopplane[i_seg]]
;size_sino  = [(size(sinogram))[1],segplanes[i_seg],nrangles,1UL]
;niswyz, sinoproj

;if keyword_set(backproject) ne 1 then stop
if keyword_set(subonly)              $
  then sinoproj = sinogram           $
else sinoproj = sinogram[*, subset, *]

if keyword_set(backproject) then sinoproj /= n_elements(subset)

size_sino  = [(size(sinoproj))[1],(size(sinoproj))[3],(size(sinoproj))[2],1UL]
niswyz, sinoproj


; resolution modeling
if keyword_set(backproject) then begin   
    tmpimage = image * 0.0
    ; resolution in sinogram
    if fwhm_t gt 0 $
        then sinoproj = NIconvolgauss(sinoproj, fwhm = fwhm_t, dim=0)
    if fwhm_a gt 0 $
        then sinoproj = NIconvolgauss(sinoproj, fwhm = fwhm_a, dim=1)
    
endif else begin
    
   ; resoultion modelling
   if fwhm_xy gt 0 or fwhm_z gt 0 then begin
      tmpimage = NIconvolgauss(image, fwhm = fwhm_xy, dim = [0,1], nrsig = 3)
      tmpimage = NIconvolgauss(tmpimage, fwhm = fwhm_z, dim = [2], nrsig = 3)
   endif else begin
      tmpimage = image
   endelse

endelse

;stop


;start_time0 = systime(1)

;== Allocate buffers & write data
bptr_image    =  0L
bptr_sino     =  1L
bptr_srclocs0 =  2L
bptr_detbins0 =  3L
;bptr_segstart =  4L
;bptr_angles   =  4L
bptr_mc       =  4L
;bptr_debug    =  6L


b = bridge->create_buffer(bptr_image,    tmpimage,  0, 0)
b = bridge->create_buffer(bptr_sino,     sinoproj,  0, 0)
b = bridge->create_buffer(bptr_srclocs0, srclocs0,  2, 0)
b = bridge->create_buffer(bptr_detbins0, detbins0,  2, 0)
;b = bridge->create_buffer(bptr_segstart, float(segstartplane),  2, 0)
b = bridge->create_buffer(bptr_mc,       rigmotion, 2, 0)
;output = fltarr(4, nrangles)   ;sinoproj                        ; shouldn't turn on if too many views at one time
;output = fltarr(6)
;output = fltarr(size_sino[0],size_sino[1],3)
;output = fltarr(size_sino[0],size_sino[1],size_sino[2],3)
;b = bridge->create_buffer(bptr_debug, output,  0, 0)       ;write_only;


if keyword_set(backproject) then begin
  kernel = 'back'
endif else begin
  kernel = 'proj'
endelse

;== Set kernel arguments
b = bridge->set_kernel_arg(kernel, 0, bptr_image,    true)
b = bridge->set_kernel_arg(kernel, 1, bptr_sino,     true)
b = bridge->set_kernel_arg(kernel, 2, bptr_srclocs0, true)
b = bridge->set_kernel_arg(kernel, 3, bptr_detbins0, true)

b = bridge->set_kernel_arg(kernel, 4, size_img,   false)
b = bridge->set_kernel_arg(kernel, 5, size_sino,  false)
b = bridge->set_kernel_arg(kernel, 6, img_offset, false)
b = bridge->set_kernel_arg(kernel, 7, vox_size,   false)
;b = bridge->set_kernel_arg(kernel, 9, add_offset_seg, false)
b = bridge->set_kernel_arg(kernel, 8, bptr_mc,    true)
;var_deb = 0.
;b = bridge->set_kernel_arg(kernel, 10, bptr_debug, true)


;== Actual work
global = ulong(size_sino[0:2])
local  = ulong([0,0,0])

;start_time = systime(1)
b = bridge->execute_kernel(kernel, global, local, false)
;print, keyword_set(backproject), 'opencl time:', (systime(1)-start_time)/60.

;== Read out data
if keyword_set(backproject) then begin
  b = bridge->read_buffer(bptr_image, tmpimage)
  ;image /= nrangles
  
   ; resoultion modelling
   if fwhm_xy gt 0 or fwhm_z gt 0 then begin
      tmpimage = NIconvolgauss(tmpimage, fwhm = fwhm_xy, dim = [0,1], nrsig = 3)
      tmpimage = NIconvolgauss(tmpimage, fwhm = fwhm_z, dim = [2], nrsig = 3)
   endif
   image += tmpimage
endif else begin
  b = bridge->read_buffer(bptr_sino, sinoproj)

  niswyz, sinoproj
  ; resolution in sinogram
  if fwhm_t gt 0 $
      then sinoproj = NIconvolgauss(sinoproj, fwhm = fwhm_t, dim=0)
  if fwhm_a gt 0 $
      then sinoproj = NIconvolgauss(sinoproj, fwhm = fwhm_a, dim=1)
  
  if keyword_set(subonly)                $
    then sinogram             = sinoproj $
  else sinogram[*,subset,*] = sinoproj
endelse

;b = bridge->read_buffer(bptr_debug, output)

;== Clean up buffers
b = bridge->release_buffer(bptr_image)
b = bridge->release_buffer(bptr_sino)
b = bridge->release_buffer(bptr_detbins0)
b = bridge->release_buffer(bptr_srclocs0)
;b = bridge->release_buffer(bptr_segstart)
b = bridge->release_buffer(bptr_mc)
;b = bridge->release_buffer(bptr_debug)

;print, keyword_set(backproject), 'buffer + opencl time:', (systime(1)-start_time0)/60.
;stop
;print, 'hit!'




end

