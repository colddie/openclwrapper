// Projector parallel over the sinogram
//
// Following parameters are defined in the build options
//
// FAST_MATH		: allows optimizations which are not IEEE 754 compatible
// BACK_PROJECT		: use backprojection instead of projection
// RUN_ON_CPU		: use CPU specific code
// TOMOSYNTHESIS	: assume simplified tomo geometry
// CBCT             : CBCT described by nidef_pmocl
//
// detbins really contains the centers of the last and first detector elements, which is different from DD projector!

#ifdef BACK_PROJECT
	// Help function: atomic add for float
	// -- This gives a HUGE slowdown, but prevents race conditions
	inline void AtomicAdd(volatile __global float *source, const float operand)
	{
		union{unsigned int intVal; float floatVal;} newVal;
		union{unsigned int intVal; float floatVal;} oldVal;

		do {
			oldVal.floatVal = *source;
			newVal.floatVal = oldVal.floatVal + operand;;
		}
		while (atomic_cmpxchg((volatile __global unsigned int *)source, oldVal.intVal, newVal.intVal) != oldVal.intVal);
	}
#endif /* BACK_PROJECT */

// Actual projector / backprojector
//
__kernel void main_kernel(	__global float*		image,
							__global float*		sinogram,
							__global float*		detbins,
							__global float*		srclocs,
							__private float4    center,
							__private uint4		size_img,
							__private uint4		size_sino,
							__private float4	img_offset,
							__private float4	vox_size, 
							__global float*     rigmotion)

{
	// Initialization of constants and variables
	const	uint	idx		= get_global_id(0);
	const	uint	idy		= get_global_id(1);
	const	uint	idz		= get_global_id(2);
	const	uint	index	= idz * size_sino.x * size_sino.y + idy * size_sino.x + idx;

	bool	temp_bool;
	bool	temp_boolx;
	float	temp_float;	
	float	temp_debug = 1024.0f;
	uint4	temp_uint4;
	float4	temp_float4;
	int		temp_int;
	
	int		this_vox_x;
	int		this_vox_y;
	int		this_vox_z;
			
	uint	memo1;
	uint	memo2;
	uint	memo3;
			
	uint4	mem_offset = {1, size_img.x, size_img.x * size_img.y, 0};
	uint4	sortx = (uint4)(0,1,2,3);
	uint4	sorty = (uint4)(0,1,2,3);
			
	float	weight1;
	float	weight2;
	float	weight3;
	float	sino_value;
			
	float4	weight0 = {0.0f, 0.0f, 0.0f, 0.0f};
	float4	plane_n = {0.0f, 0.0f, 1.0f, 0.0f}; // Vector coordinate of plane: normal vector
	float4	plane_p = {0.0f, 0.0f, 0.0f, 0.0f}; // Vector coordinate of plane: point in plane	
	int4	xy_index;
	float4	xy_projdet;
	float4  corner;
	float4	corner1;
	float4	corner2;
	float4	corner3;
	float4	corner4;
	float4	source;
	//float4	c1_inplane;
	//float4	c2_inplane;
	//float4	c3_inplane;
	//float4	c4_inplane;
	float4	c1_inplane0;
	float4	c2_inplane0;
	//float4	c3_inplane0;
	//float4	c4_inplane0;
	float4  c_inplane;
	float4  c_inplane0;
	float4	inv_vox_size;
	
	float4 hrow1;
	float4 hrow2;
	float4 hrow3;
	float4 hrow4;
	//float4	corner1_0;
	//float4	corner2_0;
	//float4	corner3_0;
	//float4	corner4_0;
	float4  corner_0;
	float4	source_0;

	// Check if the given coordinates are inside the sinogram, continue if this is the case
	temp_bool = (idx < size_sino.x && idy < size_sino.y && idz < size_sino.z);
	if (temp_bool)
	{
		// Get the coordinates of the bottom left and top right corners of the detector pixel and of the point source
		temp_uint4 = (uint4)(0,1,2,3) + idz * (uint4)(4,4,4,4);


		corner1 = vload4(temp_uint4.x, detbins);
		corner2 = vload4(temp_uint4.y, detbins);
		corner3 = vload4(temp_uint4.z, detbins);
		corner4 = vload4(temp_uint4.w, detbins);
		source  = vload4(idz, srclocs);

		// Calculate detector center individually for CBCT	
		temp_float  = (idx + 0.0f) / (size_sino.x-1);
		c1_inplane0 = mad((float4)(temp_float), corner4 - corner1, corner1);
		c2_inplane0 = mad((float4)(temp_float), corner3 - corner2, corner2);
		temp_float	= (idy + 0.0f) / (size_sino.y-1);
		corner 		= mad((float4)(temp_float), c2_inplane0 - c1_inplane0, c1_inplane0);			
		//temp_float  = (idx + 0.0f) / size_sino.x;
		//c1_inplane0 = mad((float4)(temp_float), corner4 - corner1, corner1);
		//c2_inplane0 = mad((float4)(temp_float), corner3 - corner2, corner2);
		//temp_float  = (idx + 1.0f) / size_sino.x;
		//c3_inplane0 = mad((float4)(temp_float), corner3 - corner2, corner2);
		//c4_inplane0 = mad((float4)(temp_float), corner4 - corner1, corner1);
		//temp_float	= (idy + 0.0f) / size_sino.y;
		//corner1		= mad((float4)(temp_float), c2_inplane0 - c1_inplane0, c1_inplane0);
		//corner4		= mad((float4)(temp_float), c3_inplane0 - c4_inplane0, c4_inplane0);
		//temp_float	= (idy + 1.0f) / size_sino.y;
		//corner2		= mad((float4)(temp_float), c2_inplane0 - c1_inplane0, c1_inplane0);
	    //corner3		= mad((float4)(temp_float), c3_inplane0 - c4_inplane0, c4_inplane0);


		#ifdef MC
         temp_uint4 = (uint4)(0,1,2,3) + idz * (uint4)(4,4,4,4);
         //hmatrix = vload16(idz*8, rigmotion);
         hrow1 = vload4(temp_uint4.x, rigmotion);
         hrow2 = vload4(temp_uint4.y, rigmotion);
         hrow3 = vload4(temp_uint4.z, rigmotion);
         hrow4 = vload4(temp_uint4.w, rigmotion);
         
         // first applying translation
         temp_float4 = (float4)(hrow1.w, hrow2.w, hrow3.w, 0.0f);
         corner += temp_float4          - center;
         source  += temp_float4          - center;   
           
         hrow1.w = 0.0f;
         hrow2.w = 0.0f;
         hrow3.w = 0.0f;                   
         
         // then rotation                                
         corner_0.x = dot(corner, hrow1);  
         corner_0.y = dot(corner, hrow2); 
         corner_0.z = dot(corner, hrow3);          
         
         source_0.x  = dot(source, hrow1);   
         source_0.y  = dot(source, hrow2); 
         source_0.z  = dot(source, hrow3);
       
         corner = corner_0      + center;
         //corner2 = corner2_0      + center;
         //corner3 = corner3_0      + center;
         //corner4 = corner4_0      + center;
         source  = source_0       + center;
         #endif 
		
		
		
		// Read sinogram value for future use
		#ifdef BACK_PROJECT
			sino_value = sinogram[index];
		#else
			sino_value = 0.0f;
		#endif /* BACK_PROJECT */

		// Check the intersection angle of the current ray with the volume
		// Do a coordinate transform if the projection lines don't intersect the voxel through the top
			#ifdef FAST_MATH
				temp_float4 = fabs(fast_normalize(corner - source));
			#else
				temp_float4 = fabs(normalize(corner - source));
			#endif

			temp_float  = fmax(fmax(temp_float4.x, temp_float4.y), temp_float4.z);

			if (temp_float4.z != temp_float)
			{
				temp_uint4 = (temp_float4.y == temp_float)?(uint4)(2,0,1,3):(uint4)(1,2,0,3);
				// Rearrange all vectors
				size_img   = shuffle(size_img,   temp_uint4);
				img_offset = shuffle(img_offset, temp_uint4);
				mem_offset = shuffle(mem_offset, temp_uint4);
				source     = shuffle(source,     temp_uint4);
				corner     = shuffle(corner,     temp_uint4);
				vox_size   = shuffle(vox_size,   temp_uint4);
			}
			// else: default situation, mask = (0,1,2,3), no shuffling
	


		// Calculate the path lenght through the plane: (1/cos(alpha)*cos(gamma)) in paper
		temp_float4  = corner - source;
		temp_float4 /= temp_float4.z;
		
		#ifdef FAST_MATH
			weight1 = vox_size.z * fast_length(temp_float4);
		#else
			weight1 = vox_size.z * length(temp_float4);
		#endif

		// Transform corner vectors for later use
		corner -= source;
		
		// Calculate intersection with first plane, 
		// and vector shift to next planes
		
		// Change the vector z-coordinate of the plane to the plane in the first loop
	    plane_p.z =  img_offset.z;                     // the starting plane is 0

		// Get the intersections of lines connecting source to detector pixel corners with current plane
		temp_float = dot(plane_n, (plane_p - source));

		#ifdef FAST_MATH
			c_inplane0 = mad((float4)(native_divide(temp_float, dot(plane_n, corner))), corner, source);
		#else
			c_inplane0 = (temp_float / dot(plane_n, corner)) * corner + source;
		#endif
		
		// Calculate vector shifts for the intersections
		// use the cornerN variables for this
		#ifdef FAST_MATH
			corner *= vox_size.z * native_recip(corner.z);
		#else
			corner *= (vox_size.z / corner.z);
		#endif
		
		// Normalization factors for the projection weights
		#ifdef FAST_MATH
			inv_vox_size = native_recip(vox_size.xxyy);
		#else
			inv_vox_size = 1.0f / vox_size.xxyy;
		#endif /* FAST_MATH */

		// Loop over all planes of the image volume
		for (this_vox_z = 0; this_vox_z < size_img.z; this_vox_z++)
		{
			// Memory offset for this plane
			memo1 = mem_offset.z * this_vox_z;
			
			// Change the vector z-coordinate of the plane to the plane in this loop
			#ifdef FAST_MATH
				c_inplane = mad((float4)(this_vox_z), corner, c_inplane0);
			#else
				c_inplane = this_vox_z * corner + c_inplane0;
			#endif /* FAST_MATH */
		
			// Check in which image voxel (index) the projection line arrived
			xy_projdet = (c_inplane.xxyy - img_offset.xxyy) * inv_vox_size;
			//xy_index = (int4)(floor(xy_projdet.x), ceil(xy_projdet.y), floor(xy_projdet.z), ceil(xy_projdet.w));
			temp_float4 = floor(xy_projdet) + (float4)(0.0f,1.0f,0.0f,1.0f);
			xy_index = (int4)(temp_float4.x,temp_float4.y,temp_float4.z,temp_float4.w);
			
			// There are only 4 weights to be calculated, all others are equal to 1 / projeted detector pitch
			// This means it can be removed from the double for loop
			weight0 = (float4)(1.0f, 1.0f, 1.0f, 1.0f) - fabs(xy_projdet - (float4)(xy_index.x,xy_index.y,xy_index.z,xy_index.w));
			//weight0 = (float4)(1.0f, 0.0f, 1.0f, 0.0f) + ((float4)(xy_index.x,xy_index.y,xy_index.z,xy_index.w)-xy_projdet)*(float4)(1.0f,-1.0f,1.0f,-1.0f);
            //weight0.x   = 1.0f - xy_projdet.x + (float)(xy_index.x);
			//weight0.y   = 1.0f - weight0.x;
			//weight0.z   = 1.0f - xy_projdet.z + (float)(xy_index.z);
			//weight0.w   = 1.0f - weight0.z;
			
			
			// Check if vox_y is within a valid range
			temp_bool = (xy_index.z >= 0 && xy_index.z < size_img.y);
			if(temp_bool){
			  memo2 = memo1 + xy_index.z * mem_offset.y;
			  temp_bool = (xy_index.x >= 0 && xy_index.x < size_img.x);
			  if(temp_bool){			  
			    weight3  = weight0.z * weight1 * weight0.x; 
			    #ifdef BACK_PROJECT
			    	AtomicAdd(&image[memo2 + xy_index.x * mem_offset.x], weight3 * sino_value);
			    #else
				    sino_value += weight3 * image[memo2 + xy_index.x * mem_offset.x];
			    #endif /* BACK_PROJECT */
			  }
			  temp_bool = (xy_index.y >= 0 && xy_index.y < size_img.x);
			  if(temp_bool){
			    weight3  = weight0.z * weight1 * weight0.y;
			    #ifdef BACK_PROJECT
				    AtomicAdd(&image[memo2 + xy_index.y * mem_offset.x], weight3 * sino_value);
			    #else
			    	sino_value += weight3 * image[memo2 + xy_index.y * mem_offset.x];
			    #endif /* BACK_PROJECT */
			  }
			}
			
			temp_bool = (xy_index.w >= 0 && xy_index.w < size_img.y);
			if(temp_bool){
			  memo2 = memo1 + xy_index.w * mem_offset.y;
			  temp_bool = (xy_index.x >= 0 && xy_index.x < size_img.x);
			  if(temp_bool){	
			    weight3  = weight0.w * weight1 * weight0.x; 
			    #ifdef BACK_PROJECT
			    	AtomicAdd(&image[memo2 + xy_index.x * mem_offset.x], weight3 * sino_value);
			    #else
			    	sino_value += weight3 * image[memo2 + xy_index.x * mem_offset.x];
			    #endif /* BACK_PROJECT */
			  }
			  temp_bool = (xy_index.y >= 0 && xy_index.y < size_img.x);
			  if(temp_bool){	
			    weight3  = weight0.w * weight1 * weight0.y;
			    #ifdef BACK_PROJECT
			    	AtomicAdd(&image[memo2 + xy_index.y * mem_offset.x], weight3 * sino_value);
			    #else
			    	sino_value += weight3 * image[memo2 + xy_index.y * mem_offset.x];
			    #endif /* BACK_PROJECT */
			  }
			}
			
			//// Loop over the projected voxels in the y-direction
			//for (this_vox_y = xy_index.z; this_vox_y <= xy_index.w; this_vox_y++)
			//{
			//	// Check if vox_y is within a valid range
			//	temp_bool = (this_vox_y >= 0 && this_vox_y < size_img.y);
			//	if(!temp_bool){continue;}
			//	memo2 = memo1 + this_vox_y * mem_offset.y;					
//
			//	// Assign the correct weight for y
			//	weight2  = (this_vox_y == xy_index.z)?weight0.z:1.0f;
			//	weight2 *= (this_vox_y == xy_index.w)?weight0.w:1.0f;
			//	weight2 *= weight1;
			//	
			//	// Loop over the projected voxels in the x-direction
			//	for (this_vox_x = xy_index.x; this_vox_x <= xy_index.y; this_vox_x++)
			//	{
			//		// Check if vox_x is within a valid range
			//		temp_boolx = (temp_bool && this_vox_x >= 0 && this_vox_x < size_img.x);			
			//		if(!temp_boolx){continue;}
//
			//		// Assign the correct weight for x
			//		weight3  = (this_vox_x == xy_index.x)?weight0.x:1.0f;
			//		weight3 *= (this_vox_x == xy_index.y)?weight0.y:1.0f;				
			//		weight3 *= weight2;
//
			//		#ifdef BACK_PROJECT
			//			AtomicAdd(&image[memo2 + this_vox_x * mem_offset.x], weight3 * sino_value);
			//		#else
			//			// Add value of current voxel to ray sum
			//			sino_value += weight3 * image[memo2 + this_vox_x * mem_offset.x];
			//			//if((memo2 + this_vox_x * mem_offset.x)>sino_value){sino_value = memo2 + this_vox_x * mem_offset.x;}
			//			//sino_value += weight3 * image[memo2 + clamp(this_vox_x, (int)0, (int)(size_img.x-1)) * mem_offset.x];
			//		#endif /* BACK_PROJECT */
					
			//	}// endfor over x
		//	}// endfor over y
		}// enfor over z
		#ifndef BACK_PROJECT
			//sinogram[index] = temp_debug;
			#ifdef EXPMIN
				#ifdef FAST_MATH
					sino_value = native_exp(-sino_value);
				#else
					sino_value = exp(-sino_value);
				#endif /* FAST_MATH */
			#endif /* EXPMIN */
			//if (index == 1024){sino_value = 0.0f;}
			sinogram[index] = sino_value;
		#endif /* BACK_PROJECT */
	}// endif to check valid range of idx, idy and idz
}