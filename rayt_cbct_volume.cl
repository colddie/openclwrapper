// Backprojector parallel over the volume
//
// -- Currently only valid for tomosynthesis backprojection
//
// Following parameters are defined in the build options
//
// TOMOSYNTHESIS    : assume simplified tomo geometry
// FAST_MATH		: allows optimizations which are not IEEE 754 compatible
// RUN_ON_CPU		: use CPU specific code
// SQUARED_WEIGHTS	: used to calculate l_{ij}^2
// CBCT             : CBCT described by nidef_pmocl
// SINO_PIX_X		: needed for CBCT calculations
// PM_REBIN			: rebin factor for the projection matrix
//

__kernel void main_kernel(	__global float*		image,
							__global float*		sinogram,
							__global float*		detbins,
							__global float*		srclocs,
                          __private float4       center,
							__private uint4		size_img,
							__private uint4		size_sino,
							__private float4	img_offset,
							__private float4	vox_size,
                          __global float*    rigmotion,
						  __global float*    output)							
                             

{
	// Initialization of constants and variables
	const uint		idx			= get_global_id(0);
	const uint		idy			= get_global_id(1);
	const uint		idz			= get_global_id(2);
	const uint		index		= idz * size_img.x * size_img.y + idy * size_img.x + idx;

	const float4	sino_offset = {0.0f, 0.0f, 0.0f, 0.0f};
	
	float4 pix_size;
	//const float4	pix_size	= {SINO_PIX_X, SINO_PIX_X, SINO_PIX_X, SINO_PIX_X};

	int4	temp_int4;
	float	temp_float;
	float	temp_debug;
	uint4	temp_uint4;
	float4	temp_float4;

	float	weight1;
	float	weight2;
	float	weight3;
	float	back_value = 0.0f;
	float	mag;

	uint	memo1;
	uint	memo2;
	uint	memo3;

	int		this_pix_x;
	int		this_pix_y;
	int		this_pix_z;

	uint4	mem_offset = {1, size_sino.x, size_sino.x * size_sino.y, 0};

	float4	weight0 = {0.0f, 0.0f, 0.0f, 0.0f};
	float4	xy_index;
	float4	xy_projvox;
	float4	plane_n = {0.0f, 0.0f, 1.0f, 0.0f}; // Vector coordinate of plane: normal vector
	float4	plane_p = {0.0f, 0.0f, sino_offset.z, 0.0f}; // Vector coordinate of plane: point in plane
	float4	source;

	float4	corner1;
	//float4	corner2;
	//float4	corner3;
	//float4	corner4;

	float4	c1_inplane;
	//float4	c2_inplane;
	//float4	c3_inplane;
	//float4	c4_inplane;


	float4	detcorner1;
	float4	detcorner2;
	float4	detcorner3;
	float4	detcorner4;	
	float4 detcorner1_0;
	float4	pm1;
	float4	pm2;
	//float4	pm3;
	float   PM_REBIN = 1.0f;
	float   SINO_PIX_X;

	
	// Check if the given coordinates are inside the volume, continue if this is the case
	if (idx < size_img.x && idy < size_img.y && idz < size_img.z)	
    //if (idx == 40 && idy == 40 && idz == 30)		
	{	
		// Loop over the sinogram z-direction (source positions / angles)
		for (this_pix_z = 0; this_pix_z < size_sino.z; this_pix_z++)	
			//for (this_pix_z = 0; this_pix_z < 1; this_pix_z++)
		{
			// Memory offset for this plane
			memo1 = mem_offset.z * this_pix_z;
		    temp_uint4 = (uint4)(0,1,2,3) + this_pix_z * (uint4)(4,4,4,4);
			// Get the current source position
			//source = vload4(this_pix_z, srclocs);
			
			// Detector corners for CBCT
			    detcorner1 = vload4(temp_uint4.x, detbins);
		        detcorner2 = vload4(temp_uint4.y, detbins);
		        detcorner3 = vload4(temp_uint4.z, detbins);
		        detcorner4 = vload4(temp_uint4.w, detbins);
		        source  = vload4(this_pix_z, srclocs);
				//temp_uint4 = (uint4)(0,1,2,3) + this_pix_z * (uint4)(8,8,8,8);
				//detcorner1 = vload4(temp_uint4.x, detbins);
				//detcorner2 = vload4(temp_uint4.y, detbins);
				//detcorner3 = vload4(temp_uint4.z, detbins);
				//detcorner4 = vload4(temp_uint4.w, detbins);
				//temp_uint4 += (uint4)(4,4,4,4);
				//pm1 = vload4(temp_uint4.x, detbins);
				//pm2 = vload4(temp_uint4.y, detbins);
				//pm3 = vload4(temp_uint4.z, detbins);
				
				temp_float = distance(detcorner1, detcorner4)/(size_sino.x-1);
				pix_size = (float4)(temp_float, temp_float, temp_float, temp_float);
					
				
			// Determine the direction through the volume (X or Y), get path length and set voxel corners
				temp_float4 = (detcorner1 + detcorner2 + detcorner3 + detcorner4) * 0.25f - source;
				if(fabs(temp_float4.x) > fabs(temp_float4.y)){
					corner1 = (float4)(idx + 0.5f, idy, idz, 0.0f) * vox_size + img_offset;
					//corner2 = (float4)(0.0f, vox_size.y, 0.0f,       0.0f) + corner1;
					//corner3 = (float4)(0.0f, 0.0f,       vox_size.z, 0.0f) + corner1;
					//corner4 = (float4)(0.0f, vox_size.y, vox_size.z, 0.0f) + corner1;
					
					
					
					//temp_float4	 = (corner1 + corner4) * 0.5f - source;
					temp_float4	 = corner1 - source;//??
					temp_float4 /= temp_float4.x;
					temp_float   = vox_size.x;
				} else {
					corner1 = (float4)(idx, idy + 0.5f, idz, 0.0f) * vox_size + img_offset;
					//corner2 = (float4)(vox_size.x, 0.0f, 0.0f,       0.0f) + corner1;
					//corner3 = (float4)(0.0f,       0.0f, vox_size.z, 0.0f) + corner1;
					//corner4 = (float4)(vox_size.x, 0.0f, vox_size.z, 0.0f) + corner1;
					
					
					
					temp_float4	 = corner1 - source;//??
					//temp_float4	 = (corner1 + corner4) * 0.5f - source;
					temp_float4 /= temp_float4.y;
					temp_float   = vox_size.y;
				}								
				#ifdef FAST_MATH
					mag = native_divide(fast_distance(source, temp_float4), fast_distance(source, corner1));
					weight1 = temp_float * fast_length(temp_float4);
				#else
					mag = distance(source, temp_float4) / distance(source, corner1);
					weight1 = temp_float * length(temp_float4);
				#endif /* FAST_MATH */
		

			// Get the intersections of lines connecting source to current voxel with detector plane
			// Determine starting and stopping X and Y coordinates and pixel indices
			
			 // => CBCT / more or less general???
				// Transform world coordinates to detector coordinates
				corner1.w = 1.0f;
				//corner2.w = 1.0f;
				//corner3.w = 1.0f;
				//corner4.w = 1.0f;
				
				temp_float4   = (detcorner1+detcorner2+detcorner3+detcorner4)/4;
			    detcorner1_0  = (float4)(-distance(detcorner1,detcorner4)/2, -distance(center,temp_float4),-distance(detcorner1,detcorner2)/2,1.0f);
				temp_float    = (detcorner1.x*detcorner1.x+detcorner1.y*detcorner1.y);
				temp_float4.x = (detcorner1_0.y*detcorner1.y+detcorner1_0.x*detcorner1.x)/temp_float; 
				temp_float4.y = (detcorner1_0.y*detcorner1.x-detcorner1_0.x*detcorner1.y)/temp_float;
				//temp_float4.z = 1.0f;
				//temp_float4.w = 1.0f;
								
			    pm1 = (float4)(temp_float4.x, -temp_float4.y, 0.0f, 0.0f);
				pm2 = (float4)(temp_float4.y, temp_float4.x, 0.0f, 0.0f);
				//pm3 = {temp_float4.x, -temp_float4.y, 1.0f, 1.0f};
				
				
				c1_inplane.x = dot(pm1, corner1);//??
				c1_inplane.y = dot(pm2, corner1);
				c1_inplane.z = corner1.z;
				//c1_inplane.z = dot(pm3, corner1);
				c1_inplane  /= c1_inplane.z;
				
				
				
				
			   /* temp_float4	 = c1_inplane * c1_inplane.z - source;//??
				//temp_float4 /= temp_float4.y;
				temp_float   = vox_size.y;
				#ifdef FAST_MATH
					mag = native_divide(fast_distance(source, temp_float4), fast_distance(source, corner1));
					weight1 = temp_float * fast_length(temp_float4)/ temp_float4.y;
				#else
					mag = distance(source, temp_float4) / distance(source, corner1);
					weight1 = temp_float * length(temp_float4)/ temp_float4.y;
				#endif  
				
				//					output[0]   = corner1.x;
                //   output[1] = corner1.y;
                //  output[2] = corner1.z;
                //   output[3] = c1_inplane.x;
				//   	output[4]   = c1_inplane.y;
                //   output[5] = c1_inplane.z;
                //  output[6] = weight1;
                //   output[7] = mag; 
				
				
				//c2_inplane.x = dot(pm1, corner2);
				//c2_inplane.y = dot(pm2, corner2);
				//c2_inplane.z = dot(pm3, corner2);
				//c2_inplane  /= c2_inplane.z;				
				//c3_inplane.x = dot(pm1, corner3);
				//c3_inplane.y = dot(pm2, corner3);
				//c3_inplane.z = dot(pm3, corner3);
				//c3_inplane  /= c3_inplane.z;	
				//c4_inplane.x = dot(pm1, corner4);
				//c4_inplane.y = dot(pm2, corner4);
				//c4_inplane.z = dot(pm3, corner4);
				//c4_inplane  /= c4_inplane.z;

				//// Sort corners (corners 1 and 4 cannot both be 2 smallest or 2 largest values)
				//temp_float4 = (float4)(c1_inplane.x, c2_inplane.x, c3_inplane.x, c4_inplane.x);
				//temp_float4 = (c1_inplane.x < c4_inplane.x)?temp_float4:temp_float4.s3120;
				//temp_float4 = (c2_inplane.x < c3_inplane.x)?temp_float4:temp_float4.s0213;

				//xy_projvox.xy = (temp_float4.xz + temp_float4.yw) * 0.5f;

				//temp_float4 = (float4)(c1_inplane.y, c2_inplane.y, c3_inplane.y, c4_inplane.y);
				//temp_float4 = (c1_inplane.y < c4_inplane.y)?temp_float4:temp_float4.s3120;
				//temp_float4 = (c2_inplane.y < c3_inplane.y)?temp_float4:temp_float4.s0213;

				//xy_projvox.zw = (temp_float4.xz + temp_float4.yw) * 0.5f;

				xy_projvox  = (float4)(c1_inplane.x, c1_inplane.x, c1_inplane.y, c1_inplane.y);
				// Determine coordinates ?? any offset on these values ??
				xy_index    = floor(xy_projvox * PM_REBIN);
				xy_projvox *= pix_size.xxyy * PM_REBIN;

	
			// There are only 4 weights to be calculated, all others are equal to 1
		    #ifdef FAST_MATH
				temp_float4 = mad(xy_index, pix_size.xxyy, sino_offset.xxyy);
			//#else
				temp_float4 = xy_index * pix_size.xxyy + sino_offset.xxyy;
			#endif /* FAST_MATH */
			//weight0 = min(temp_float4 + pix_size.xxyy, xy_projvox.yyww) - max(temp_float4, xy_projvox.xxzz);
 
            weight0 = fabs(fabs(temp_float4)- fabs(xy_projvox));
			//temp_float4 = xy_index.yyww - xy_index.xxzz - (float4)(1.0f,1.0f,1.0f,1.0f);
			//temp_float4 = max(temp_float4, (float4)(0.0f,0.0f,0.0f,0.0f));

			#ifdef CBCT
				weight0 /= pix_size.xxyy;
				weight0 *= mag;
			#endif /* CBCT */

					output[0]   = xy_index.x;
                   output[1] = xy_index.y;
                  output[2] = xy_index.z;
                   output[3] = xy_index.w;
				   	output[4]   = temp_float4.x;
                   output[5] = temp_float4.y;
                  output[6] = temp_float4.z;
                   output[7] = temp_float4.w;
				   output[8] = xy_projvox.x;
				   output[9] = xy_projvox.y;
				   output[10] = xy_projvox.z;
				   output[11] = xy_projvox.w;
				   output[12] = weight0.x;
				   output[13] = weight0.y;
				   output[14] = weight0.z;
				   output[15] = weight0.w;

			// Loop over the projected voxels in the y-direction
			for (this_pix_y = xy_index.z; this_pix_y <= xy_index.w; this_pix_y++)
			{
				// Check if vox_x is within a valid range
				if(this_pix_y < 0 || this_pix_y >= size_sino.y){continue;}

				memo2 = memo1 + this_pix_y * mem_offset.y;

				// Assign the correct weight for y
				weight2  = (this_pix_y == xy_index.z)?weight0.z:1.0f;
				weight2 *= (this_pix_y == xy_index.w)?weight0.w:1.0f;
				weight2 *= weight1;

				// Loop over the projected voxels in the x-direction
				for (this_pix_x = xy_index.x; this_pix_x <= xy_index.y; this_pix_x++)
				{
					// Check if vox_y is within a valid range
					if(this_pix_x < 0 || this_pix_x >= size_sino.x){continue;}
					
					// Assign the correct weight for x
					weight3  = (this_pix_x == xy_index.x)?weight0.x:1.0f;
					weight3 *= (this_pix_x == xy_index.y)?weight0.y:1.0f;
					weight3 *= weight2;

					#ifdef SQUARED_WEIGHTS
						weight3 *= weight3;
					#endif /* SQUARED_WEIGHTS */
					
					// Get memory offset for current sinogram pixel
					//memo3 = memo2 + this_pix_x

					// Add value of current pixel to volume
					// (sinogram for cardio CBCT is reversed in x)
					#ifdef CBCT
						back_value += weight3 * sinogram[memo2 + size_sino.x - this_pix_x - 1];
						output[16] = memo1;
						output[17] = memo2;
						output[18] = weight2;
						output[19] = weight3;
						//if((memo2 + size_sino.x - this_pix_x - 1)>back_value){back_value = memo2 + size_sino.x - this_pix_x - 1;}
					#endif /* CBCT */

				}// endfor over x
			}// endfor over y
		}// endfor over z
		image[index] = back_value;//temp_debug;//
	}// endif to check valid range of idx, idy, idz
}